import React from 'react';
import {Button, Center, Text} from 'native-base';

const WelcomeScreen = props => {
  const {navigation} = props;
  return (
    <>
      <Center flex={1}>
        <Text>Sign in through Okta</Text>
        <Button onPress={() => navigation.navigate('Login')}>
          Sign In with Okta
        </Button>
      </Center>
    </>
  );
};

export default WelcomeScreen;
