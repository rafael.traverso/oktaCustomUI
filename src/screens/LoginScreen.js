import React, {useEffect, useState} from 'react';
import {
  Button,
  Center,
  Heading,
  HStack,
  Icon,
  Input,
  Spinner,
  Stack,
} from 'native-base';
import {
  EventEmitter,
  signIn,
  createConfig,
  isAuthenticated,
} from '@okta/okta-react-native';
import Error from '../components/Error';
import configFile from '../../auth.config';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as authActions from '../store/actions/auth/auth';

const LoginScreen = props => {
  const {actions} = props;
  const {error, authenticated} = props.auth;
  const [show, setShow] = useState(false);
  const [state, setState] = useState({
    progress: false,
    username: '',
    password: '',
  });

  useEffect(() => {
    EventEmitter.addListener('signInSuccess', (e: Event) => {
      actions.setAuthenticated(true);
      actions.setContext('Logged in!');
    });
    EventEmitter.addListener('signOutSuccess', (e: Event) => {
      actions.setAuthenticated(false);
      actions.setContext('Logged out!');
    });
    EventEmitter.addListener('onError', (e: Event) => {
      console.warn(e);
      actions.setContext(e.error_message);
    });
    EventEmitter.addListener('onCancelled', (e: Event) => {
      console.warn(e);
    });
    (async () => {
      await createConfig({
        clientId: configFile.oidc.clientId,
        redirectUri: configFile.oidc.redirectUri,
        endSessionRedirectUri: configFile.oidc.endSessionRedirectUri,
        discoveryUri: configFile.oidc.discoveryUri,
        scopes: configFile.oidc.scopes,
        requireHardwareBackedKeyStore:
          configFile.oidc.requireHardwareBackedKeyStore,
      });
      await checkAuthentication();
    })();
    return () => {
      EventEmitter.removeAllListeners('signInSuccsess');
      EventEmitter.removeAllListeners('signOutSuccsess');
      EventEmitter.removeAllListeners('onError');
      EventEmitter.removeAllListeners('onCancelled');
    };
  }, []);

  const checkAuthentication = async () => {
    const result = await isAuthenticated();
    if (result.authenticated !== authenticated) {
      actions.setAuthenticated(result.authenticated);
    }
  };

  const login = () => {
    if (state.progress == true) return;
    setState({...state, progress: true});

    const {username, password} = state;
    const {navigation} = props;

    signIn({username, password})
      .then(_token => {
        setState({
          progress: false,
          username: '',
          password: '',
        });
        navigation.navigate('BTNavigator');
      })
      .catch(error => {
        setState({
          progress: false,
          username: '',
          password: '',
        });
        actions.setError(error.message);
      });
  };
  const showPassword = () => setShow(!show);

  return (
    <>
      <Center flex={1}>
        {state.progress ? (
          <HStack space={2} alignItems="center">
            <Spinner accessibilityLabel="Loading posts" />
            <Heading color="primary.500" fontSize="md">
              Loading
            </Heading>
          </HStack>
        ) : null}
        <Error error={error} />
        <Stack space={4} w="100%" maxWidth="300">
          <Input
            size="xl"
            placeholder="email"
            onChangeText={username => setState({...state, username: username})}
          />
          <Input
            type={show ? 'text' : 'password'}
            overflow="visible"
            size="xl"
            InputRightElement={
              <Button roundedLeft="0" onPress={showPassword}>
                {show ? 'Hide' : 'Show'}
              </Button>
            }
            placeholder="Password"
            onChangeText={password => setState({...state, password: password})}
          />
          <Button onPress={login}>Login</Button>
        </Stack>
      </Center>
    </>
  );
};

LoginScreen.propTypes = {
  auth: PropTypes.object,
  actions: PropTypes.object,
  navigation: PropTypes.object,
  error: PropTypes.string,
  authenticated: PropTypes.bool,
};

const mapStateToProps = state => ({
  auth: state.auth,
});

const ActionCreators = Object.assign({}, authActions);
const mapDispatchtoProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});
export default connect(mapStateToProps, mapDispatchtoProps)(LoginScreen);
