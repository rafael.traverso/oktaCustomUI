import React from 'react';
import {Center, Spinner, Text} from 'native-base';

const SplashScreen = () => {
  return (
    <>
      <Center flex={1}>
        <Spinner size="lg" />
        <Text>Loading...</Text>
      </Center>
    </>
  );
};

export default SplashScreen;
