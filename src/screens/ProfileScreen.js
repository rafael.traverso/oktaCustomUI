import React, {useState, useEffect} from 'react';
import {Box, Button, Center, Stack, Text} from 'native-base';
import {getUser, getAccessToken, clearTokens} from '@okta/okta-react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as authActions from '../store/actions/auth/auth';
import PropTypes from 'prop-types';

const ProfileScreen = props => {
  const {navigation, actions} = props;
  const {accessToken, user, error, context} = props.auth;

  useEffect(() => {
    getUser()
      .then(user => {
        actions.setUser(user);
      })
      .catch(e => {
        actions.setError(e.message);
      });
  }, []);

  const getAccess_Token = () => {
    getAccessToken()
      .then(token => {
        actions.setAccessToken(token.access_token);
      })
      .catch(e => {
        actions.setError(e.message);
      });
  };

  const logout = () => {
    clearTokens()
      .then(() => {
        navigation.navigate('Welcome');
      })
      .catch(e => {
        console.log(e.message);
      });
  };

  return (
    <>
      <Center flex={1}>
        <Stack space={4} w="100%" maxWidth="300">
          <Button onPress={logout}>Logout</Button>
          <Button style={{marginTop: 40}} onPress={getAccess_Token}>
            Get Access Token
          </Button>
          {accessToken && (
            <Box>
              <Text>User: {user.name}</Text>
              <Text>Access Token:</Text>
              <Text>{accessToken}</Text>
            </Box>
          )}
        </Stack>
      </Center>
    </>
  );
};

ProfileScreen.propTypes = {
  auth: PropTypes.object,
  actions: PropTypes.object,
  authenticated: PropTypes.bool,
  navigation: PropTypes.object,
  user: PropTypes.object,
  accessToken: PropTypes.object,
  error: PropTypes.string,
};

const mapStateToProps = state => ({
  auth: state.auth,
});

const ActionCreators = Object.assign({}, authActions);
const mapDispatchtoProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchtoProps)(ProfileScreen);
