import {
  SET_AUTHENTICATED,
  SET_ACCESS_TOKEN,
  SET_CONTEXT,
  SET_ERROR,
  SET_USER,
} from '../../constants/auth';

export const setAuthenticated = isAuthenticated => ({
  type: SET_AUTHENTICATED,
  payload: isAuthenticated,
});
export const setAccessToken = accessToken => ({
  type: SET_ACCESS_TOKEN,
  payload: accessToken,
});
export const setContext = context => ({
  type: SET_CONTEXT,
  payload: context,
});
export const setError = error => ({
  type: SET_ERROR,
  payload: error,
});
export const setUser = user => ({
  type: SET_USER,
  payload: user,
});
