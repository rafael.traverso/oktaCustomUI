import {
  SET_AUTHENTICATED,
  SET_ACCESS_TOKEN,
  SET_CONTEXT,
  SET_ERROR,
  SET_USER,
} from '../constants/auth';

export const authInitialState = {
  accessToken: null,
  user: null,
  authenticated: false,
  context: null,
  error: '',
};

export const authReducer = (state = authInitialState, action) => {
  switch (action.type) {
    case SET_AUTHENTICATED:
      return {
        ...state,
        authenticated: action.payload,
      };
    case SET_USER:
      return {
        ...state,
        user: action.payload,
      };
    case SET_CONTEXT:
      return {
        ...state,
        context: action.payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case SET_ACCESS_TOKEN:
      return {
        ...state,
        accessToken: action.payload,
      };
    default:
      return state;
  }
};
