export const SET_AUTHENTICATED = 'SET_AUTHENTICATED';
export const SET_CONTEXT = 'SET_CONTEXT';
export const SET_ACCESS_TOKEN = 'SET_ACCESS_TOKEN';
export const SET_ERROR = 'SET_ERROR';
export const SET_USER = 'SET_USER';
