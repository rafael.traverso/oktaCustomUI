import React, {useEffect} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginScreen from '../../screens/LoginScreen';
import {BottomTabNavigator} from './BottomTabNavigator';
import WelcomeScreen from '../../screens/WelcomeScreen';
import {createConfig, isAuthenticated} from '@okta/okta-react-native';
import configFile from '../../../auth.config';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as authActions from '../../store/actions/auth/auth';
import PropTypes from 'prop-types';

const Stack = createNativeStackNavigator();

const StackNavigator = props => {
  const {actions} = props;
  const {authenticated} = props.auth;

  const checkAuthentication = async () => {
    const result = await isAuthenticated();
    actions.setAuthenticated(result.authenticated);
  };

  useEffect(() => {
    async () => {
      await createConfig({
        clientId: configFile.oidc.clientId,
        redirectUri: configFile.oidc.redirectUri,
        endSessionRedirectUri: configFile.oidc.endSessionRedirectUri,
        discoveryUri: configFile.oidc.discoveryUri,
        scopes: configFile.oidc.scopes,
        requireHardwareBackedKeyStore:
          configFile.oidc.requireHardwareBackedKeyStore,
      });
      await checkAuthentication();
    };
  }, []);

  return (
    <Stack.Navigator
      initialRouteName={authenticated ? 'BTNavigator' : 'Welcome'}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Welcome" component={WelcomeScreen} />
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="BTNavigator" component={BottomTabNavigator} />
    </Stack.Navigator>
  );
};
StackNavigator.propTypes = {
  auth: PropTypes.object,
  actions: PropTypes.object,
  authenticated: PropTypes.bool,
};

const mapStateToProps = state => ({
  auth: state.auth,
});

const ActionCreators = Object.assign({}, authActions);
const mapDispatchtoProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchtoProps)(StackNavigator);
