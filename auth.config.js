export default {
  oidc: {
    clientId: '0oa20z395xAhrxNPm5d7',
    redirectUri: 'com.okta.dev-50145098:/callback',
    endSessionRedirectUri: 'com.okta.dev-50145098:/',
    discoveryUri: 'https://dev-50145098.okta.com/oauth2/default',
    scopes: ['openid', 'profile', 'offline_access'],
    requireHardwareBackedKeyStore: false,
  },
};
